﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace School.Models
{
    public class AvgMark
    {
        public double Value { get; set; }
        public string Subject { get; set; }
    }
}
