﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace School.Models
{
    public class SchoolClass
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<SchoolPupil> Pupils { get; set; }
        public List<SchoolMark> Marks { get; set; }
    }
}
