﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace School.ViewModels
{
    public class CreatePeopleModel
    {
        [Required(ErrorMessage  = "sdfkjsfslff")]
        [Display(Name = "Имя")]
        public string FirstName { get; set; }
        public int ClassId { get; set; }
        public IEnumerable<SelectListItem> Classes { get; set; }

        public CreatePeopleModel()
        {
            Classes = new List<SelectListItem>();
        }
    }
}
