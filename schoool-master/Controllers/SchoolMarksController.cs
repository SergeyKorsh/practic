﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using School.Data;
using School.Models;

namespace School.Controllers
{
    public class SchoolMarksController : Controller
    {
        private readonly ApplicationDbContext _context;

        public SchoolMarksController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: SchoolMarks
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.Marks.Include(s => s.Pupil);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: SchoolMarks/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var schoolMark = await _context.Marks
                .Include(s => s.Pupil)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (schoolMark == null)
            {
                return NotFound();
            }

            return View(schoolMark);
        }

        // GET: SchoolMarks/Create
        public IActionResult Create(int id)
        {
            ViewData["PupilId"] = id;
            return View();
        }

        // POST: SchoolMarks/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("LessonDate, Value,Subject,LessonOrderNumber,PupilId")] SchoolMark schoolMark)
        {
            if (ModelState.IsValid)
            {
                _context.Add(schoolMark);
                await _context.SaveChangesAsync();
                return RedirectToAction("Details", "SchoolPupils", new { id = schoolMark.PupilId});
            }
            return View(schoolMark);
        }

        // GET: SchoolMarks/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var schoolMark = await _context.Marks.FindAsync(id);
            if (schoolMark == null)
            {
                return NotFound();
            }
            ViewData["PupilId"] = new SelectList(_context.Pupils, "Id", "Address", schoolMark.PupilId);
            return View(schoolMark);
        }

        // POST: SchoolMarks/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Value,Subject,LessonDate,LessonOrderNumber,PupilId")] SchoolMark schoolMark)
        {
            if (id != schoolMark.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(schoolMark);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!SchoolMarkExists(schoolMark.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["PupilId"] = new SelectList(_context.Pupils, "Id", "Address", schoolMark.PupilId);
            return View(schoolMark);
        }

        // GET: SchoolMarks/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var schoolMark = await _context.Marks
                .Include(s => s.Pupil)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (schoolMark == null)
            {
                return NotFound();
            }

            return View(schoolMark);
        }

        // POST: SchoolMarks/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var schoolMark = await _context.Marks.FindAsync(id);
            _context.Marks.Remove(schoolMark);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool SchoolMarkExists(int id)
        {
            return _context.Marks.Any(e => e.Id == id);
        }
    }
}
