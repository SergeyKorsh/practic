﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using School.Data;
using School.Models;

namespace School.Controllers
{
    public class NewsPostsController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<SchoolTeacher> _userManager;

        public NewsPostsController(ApplicationDbContext context,
                                   UserManager<SchoolTeacher> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        // GET: NewsPosts
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.Posts.Include(n => n.Author);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: NewsPosts/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var newsPost = await _context.Posts
                .Include(n => n.Author)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (newsPost == null)
            {
                return NotFound();
            }

            return View(newsPost);
        }

        // GET: NewsPosts/Create
        public IActionResult Create()
        {
            ViewData["AuthorId"] = new SelectList(_context.Teachers, "Id", "Id");
            return View();
        }

        // POST: NewsPosts/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Title,Content")] NewsPost newsPost)
        {
            if (ModelState.IsValid)
            {
                newsPost.PostedDate = DateTime.Now;
                newsPost.AuthorId = (await _userManager.FindByNameAsync(User.Identity.Name)).Id;
                _context.Add(newsPost);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index", "Home");
            }
            return View(newsPost);
        }

        // GET: NewsPosts/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var newsPost = await _context.Posts.FindAsync(id);
            if (newsPost == null)
            {
                return NotFound();
            }
            ViewData["AuthorId"] = new SelectList(_context.Teachers, "Id", "Id", newsPost.AuthorId);
            return View(newsPost);
        }

        // POST: NewsPosts/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Title,Content,PostedDate,AuthorId")] NewsPost newsPost)
        {
            if (id != newsPost.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(newsPost);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!NewsPostExists(newsPost.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["AuthorId"] = new SelectList(_context.Teachers, "Id", "Id", newsPost.AuthorId);
            return View(newsPost);
        }

        // GET: NewsPosts/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var newsPost = await _context.Posts
                .Include(n => n.Author)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (newsPost == null)
            {
                return NotFound();
            }

            _context.Posts.Remove(newsPost);
            await _context.SaveChangesAsync();

            return RedirectToAction("Index", "Home");
        }

        private bool NewsPostExists(int id)
        {
            return _context.Posts.Any(e => e.Id == id);
        }
    }
}
