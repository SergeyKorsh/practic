﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using School.Data;
using School.Models;

namespace School.Controllers
{
    public class HomeTasksController : Controller
    {
        private readonly ApplicationDbContext _context;

        public HomeTasksController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: HomeTasks
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.Tasks.Include(h => h.Class);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: HomeTasks/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var homeTask = await _context.Tasks
                .Include(h => h.Class)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (homeTask == null)
            {
                return NotFound();
            }

            return View(homeTask);
        }

        // GET: HomeTasks/Create
        public IActionResult Create(int? id)
        {
            ViewData["ClassId"] = id;
            return View();
        }

        // POST: HomeTasks/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Subject,Content,Deadline,ClassId")] HomeTask homeTask)
        {
            if (ModelState.IsValid)
            {
                _context.Add(homeTask);
                await _context.SaveChangesAsync();
                return RedirectToAction("Details", "SchoolClasses", new { id = homeTask.ClassId });
            }
            ViewData["ClassId"] = new SelectList(_context.Classes, "Id", "Id", homeTask.ClassId);
            return View(homeTask);
        }

        // GET: HomeTasks/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var homeTask = await _context.Tasks.FindAsync(id);
            if (homeTask == null)
            {
                return NotFound();
            }
            ViewData["ClassId"] = new SelectList(_context.Classes, "Id", "Id", homeTask.ClassId);
            return View(homeTask);
        }

        // POST: HomeTasks/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Subject,Content,Deadline,ClassId")] HomeTask homeTask)
        {
            if (id != homeTask.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(homeTask);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!HomeTaskExists(homeTask.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["ClassId"] = new SelectList(_context.Classes, "Id", "Id", homeTask.ClassId);
            return View(homeTask);
        }

        // GET: HomeTasks/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var homeTask = await _context.Tasks
                .Include(h => h.Class)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (homeTask == null)
            {
                return NotFound();
            }

            return View(homeTask);
        }

        // POST: HomeTasks/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var homeTask = await _context.Tasks.FindAsync(id);
            _context.Tasks.Remove(homeTask);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool HomeTaskExists(int id)
        {
            return _context.Tasks.Any(e => e.Id == id);
        }
    }
}
