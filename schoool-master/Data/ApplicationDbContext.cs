﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using School.Models;

namespace School.Data
{
    public class ApplicationDbContext : IdentityDbContext<SchoolTeacher>
    {
        public DbSet<SchoolTeacher> Teachers { get; set; }
        public DbSet<NewsPost> Posts { get; set; }
        public DbSet<SchoolClass> Classes { get; set; }
        public DbSet<SchoolPupil> Pupils { get; set; }
        public DbSet<SchoolMark> Marks { get; set; }
        public DbSet<HomeTask> Tasks { get; set; }
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
           // Database.EnsureDeleted();
            Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
           base.OnModelCreating(builder);
            builder.Entity<SchoolTeacher>().HasData(new SchoolTeacher()
            {
                Id = "1f414054-f0c4-4596-a4c8-41bf8b810405",
                UserName = "egor.gaychukov@mail.ru",
                NormalizedUserName = "egor.gaychukov@mail.ru".ToUpper(),
                Email = "egor.gaychukov@mail.ru",
                NormalizedEmail = "egor.gaychukov@mail.ru".ToUpper(),
                EmailConfirmed = true,
                PasswordHash = "AQAAAAEAACcQAAAAEEx5zDXNj3VhCfYtNVq4/4dSPPzu6+QXyi6RKqWMOh75a6l6hpwFloh5D5+50/Hc3w==",
                SecurityStamp = "YS4DIVUIKWBTZWNPRYBN3HBJ67MUYORT",
                ConcurrencyStamp = "a07c0a3b-8fc6-4464-9947-76e3e1f50fb5",
                FirstName = "Иван",
                MiddleName = "Иванович",
                LastName = "Иванов"
            });
            builder.Entity<NewsPost>().HasData(new NewsPost()
            {
                Id = 1,
                AuthorId = "1f414054-f0c4-4596-a4c8-41bf8b810405",
                Title = "Title",
                Content = "ConrgjrorjreoijeAQAAAAEAACcQAAAAEEx5zDXNj3VhCfYtNVq4 AQAAAAEAACcQAAAAEEx5zDXNj3VhCfYtNVq4",
                PostedDate = DateTime.Now
            });
            builder.Entity<NewsPost>().HasData(new NewsPost()
            {
                Id = 2,
                AuthorId = "1f414054-f0c4-4596-a4c8-41bf8b810405",
                Title = "Title1",
                Content = "ConrgjrorjreoijeAQAAAAEAACcQAAAAEEx5zDXNj3VhCfYtNVq4 AQAAAAEAACcQAAAAEEx5zDXNj3VhCfYtNVq4 AQAAAAEAACcQAAAAEEx5zDXNj3VhCfYtNVq4 AQAAAAEAACcQAAAAEEx5zDXNj3VhCfYtNVq4",
                PostedDate = DateTime.Now
            });
            builder.Entity<SchoolClass>().HasData(new SchoolClass
            { 
                Id = 1,
                Name = "10А"
            });
            builder.Entity<SchoolClass>().HasData(new SchoolClass
            {
                Id = 2,
                Name = "10Б"
            });
            builder.Entity<SchoolClass>().HasData(new SchoolClass
            {
                Id = 3,
                Name = "9А"
            });
            builder.Entity<SchoolPupil>().HasData(new SchoolPupil
            {
                Id = 1,
                FirstName = "Сидр",
                MiddleName = "Иванович",
                LastName = "Котов",
                ClassId = 1,
                Address = "guijerier 10,12",
                Email = "sidr@mail.ru"
            });
            builder.Entity<SchoolPupil>().HasData(new SchoolPupil
            {
                Id = 2,
                FirstName = "Андрей",
                MiddleName = "Владимирович",
                LastName = "Зайцев",
                ClassId = 1,
                Address = "dfuihdfb 20",
                Email = "andrey@mail.ru"
            });
            builder.Entity<SchoolPupil>().HasData(new SchoolPupil
            {
                Id = 3,
                FirstName = "Николай",
                MiddleName = "Дмитриевич",
                LastName = "Зайченков",
                ClassId = 1,
                Address = "dfuihdfb 20",
                Email = "andrey@mail.ru"
            });
            builder.Entity<SchoolPupil>().HasData(new SchoolPupil
            {
                Id = 4,
                FirstName = "Владимир",
                MiddleName = "Иванович",
                LastName = "Рубанов",
                ClassId = 2,
                Address = "dfuihdfb 20",
                Email = "andrey@mail.ru"
            });
            builder.Entity<SchoolPupil>().HasData(new SchoolPupil
            {
                Id = 5,
                FirstName = "Вадим",
                MiddleName = "Сергеевич",
                LastName = "Пронин",
                ClassId = 2,
                Address = "dfuihdfb 20",
                Email = "andrey@mail.ru"
            });
            builder.Entity<SchoolPupil>().HasData(new SchoolPupil
            {
                Id = 6,
                FirstName = "Глеб",
                MiddleName = "Олегович",
                LastName = "Мишин",
                ClassId = 3,
                Address = "dfuihdfb 20",
                Email = "andrey@mail.ru"
            });
        }
    }
}
